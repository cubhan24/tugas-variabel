<?php
// function number


function calculateDistance($satu, $dua)
{
  return abs($satu + $dua);
}
//jarak antara ke dua bilangan tersebut
echo calculateDistance(12, 17); //primt: 5
echo "\n";
echo calculateDistance(-1, 4); // Prints: 5
echo "\n";
echo calculateDistance(4, 1); // Prints: 3
echo "\n";
echo calculateDistance(3, 7); // Prints: 4
echo "\n";
echo calculateDistance(7, 3); // Prints: 4
echo "\n";

function calculateTip($total)
{
  return round($total * 1.18);
}

/*fungsi round() membulatkan pecahan yang paling dekat dengan bilangan aslinya 
angka 0,5 akan dibulatkan keatas , sedangkan 0,49 akan dibulatkan ke bawah.*/
echo calculateTip(100); // Prints 118 
echo "\n";
echo calculateTip(35); /* Prints 41 ,pada asalnya hasil dari 35*1.18= 41.3 karena pecahannya 
                        dibawah 0.5 maka dibulatkan ke bawah menjadi 41*/
echo "\n";
echo calculateTip(88.77); //Prints 105
echo "\n";
echo calculateTip(16); /*print 19 ,pada asalnya hasil dari 16*1.18 = 18.88 karena pecahannya diatas 0.5
maka dubulatkan menjadi 19*/
