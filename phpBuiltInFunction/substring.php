<?php
//working with substring

$paragraf_satu = "Fungsi adalah sekumpulan intruksi yang dibungkus dalam sebuah blok
Fungsi dapat digunakan ulang tanpa harus menulis ulang instruksi di dalamnya
Fungsi pada PHP dapat dibuat dngan kata kunci function, lalu diikuti dengan nama fungsinya.";

$paragraf_dua = "Supaya intruksi yang di dalam fungsi lebih dinamis, kita dapat menggunakan parameter 
untuk memasukkan sebuah nilai ke dalam fungsi. Nilai tersebut akan diolah di dalam fungsi
Misalkan, pada contoh fungsi yang tadi, tidak mungkin nilai yang dicetak adalah ardianta saja 
dan salam yang dipakai tidak selalu assalamualaikum. Maka, kita dapat menambahkan parameter ";

//kita tulis dibawah ini
/*untuk mengetahui seberapa banyak kata tersebut digunakan , besar kecilnya huruf berpengaruh 
ketika akan di hitung*/

echo substr_count($paragraf_satu,"Fungsi");
echo "\n";
echo substr_count($paragraf_dua,"nilai");